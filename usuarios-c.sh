#!/bin/bash

#Mensaje de aviso

	echo "********************************************************"
	echo "Necesitas ejecutar el menú como sudo para crear usuarios"
	echo "********************************************************"

#Comprobar si le hemos pasado un txt o no

read -p "Teclea el nombre del fichero de usuarios: " fichero

	if [ ! -f "$fichero" ]; then
	echo "El fichero $fichero no existe."
	exit 1
	fi

#Bucle para crear usuarios

		while read nombreusuario ; do

			#Comprobación de usuario
			if id "$nombreusuario" >/dev/null 2>&1; then
				echo "$nombreusuario ya existe, no se creará."
			else
			#Si no existe, lo crea
				useradd "$nombreusuario"
				echo "$nombreusuario creado."
			fi
		done< "$fichero"


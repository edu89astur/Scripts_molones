#/bin/bash

#Primero pedimos que se teclee una IP

echo "Ingresa una direccion IP, chatin:"

read ip

ping -c 3 $ip >> ping.txt

#Realizamos una  variable para comprobar si el ping se realiza con éxito

if [ $? -eq 0 ]; then

echo "Existe conexión física con la dirección IP $ip"

else 

echo "No hay conexión con la dirección IP $ip"


fi


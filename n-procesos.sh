#!/bin/bash

#Creamos las variables

usuario="$1"
procesos=$(ps -U "$usuario" | wc -l)

#Realizamos el condicional con el operador -lt que es un comparador que significa menor que.

if [ $procesos -lt 10 ]; then

echo "El usuario $usuario tiene menos de 10 procesos en ejecución."

else

echo "El usuario $usuario tiene 10 o más procesos en ejecución."

fi

#!/bin/bash

# Primero listamos el contenido del directorio

lista=$(ls)

# Creamos un bucle

for cosas in $lista; do
	if [ -f "$cosas" ]; then

#Verificamos que sea un fichero  y mostramos

	echo "Nombre del fichero: $cosas"
	cat "$cosas"
	echo "##########################"

	elif [ -d "$cosas" ]; then
# Elif nos permite añadir una variable si no se da la primera parte
# Verificamos directorio

	echo "Nombre del directorio: $cosas"
ls -l "$cosas"
fi
done

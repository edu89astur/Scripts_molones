#/bin/bash

#Realizamos un while para leer el fichero servidores.txt y que repase las IP

while read ip; do

ping -c 3 "$ip" >> /dev/null

if [ $? -eq 0 ]; then 

echo "Servidor de IP $ip: Accesible"

else

echo "Servidor de IP $ip: No accesible"

fi

done < servidores.txt

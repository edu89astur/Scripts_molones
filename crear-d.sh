#!/bin/bash

#Primero haremos una comprobación de si el parámetro es un directorio o no.
read -p "Introduce el nombre de un directorio:" directorio

	if [ ! -d $directorio ]; then
		echo "$directorio No es un directorio"
		exit 1

	fi

#Usaremos while para leer el archivo linea por linea y crear directorios

while read  nombredirectorio; do
	nuevo_directorio="$directorio/$nombredirectorio"

#Comprobamos si el directorio existe
	if [ -d "$nuevo_directorio" ]; then

		echo "El directorio $nuevo_directorio ya existe"

	else
	#Creamos directorio 

	mkdir -p "$nuevo_directorio"
	echo "Has creado el directorio $nuevo_directorio"

	#Usamos el -p para que cree la cadena de directorios necesaria si no existiese
	fi

done < crear.txt

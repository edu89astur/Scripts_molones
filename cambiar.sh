#!/bin/bash

#Pedimos el nombre de archivo

read -p "Ingresa el nombre de un archivo: " archivo

#Pedimos el nivel de permiso

read -p "Ingresa los números de permiso: " numero

#Cuidado. Al añadir "sudo" aquí, el sistema no pide contraseña para cambiar permisos

	sudo chmod $numero $archivo

	echo "Permisos cambiados con éxito."
#Mostramos los permisos del archivo

	ls -l $archivo
#Este comentario es para hacer una prueba en codeberg
exit 1


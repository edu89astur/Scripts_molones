#!/bin/bash

#Estructuramos las opciones

echo "_______________________"
echo "|   Elige una opción  |"
echo "| 1. Enlace duro      |"
echo "| 2. Enlace simbólico |"
echo "|---------------------| "

#Solicitamos la opción

read -p "Opción: " opcion

#Comenzamos a  anidar

	case $opcion in

	1)
		#Aportamos un nombre de archivo o su ruta

		read -p "Ingresa la ruta o el nombre del archivo o directorio: " ruta

		#Verificamos que exista o no

		if [ -e "$ruta" ]; then

			#Elegimos la ruta del enlace duro

			read -p "Ingresa la ruta del enlace duro: " rutadura

			#Creamos el enlace duro

			ln "$ruta" "$rutadura"

			echo "Enlace duro creado con éxito en: $rutadura"

		else
			#Si el archivo no existe o fallamos

			echo "El archivo introducido o la ruta no existen o no están correctos"

		exit 1

		fi
	;;
	2)
		#Aportamos un nombre de archivo o su ruta

		read -p "Ingresa la ruta o el nombre del archivo o directorio: " ruta

		#Comprobamos su existencia

		if [ -e "$ruta" ]; then

			#Elegimos la ruta del enlace simbólico

			read -p "Ingresa la ruta del enlace simbólico: " rutablanda

			#Creamos el enlace simbólico

			ln -s "$ruta" "$rutablanda"

			echo "Enlace simbólico creado con éxito en: $rutablanda"

		else

			#Si el archivo no existe o fallamos

			echo "El archivo introducido o la ruta no existen o no están correctos"

		exit 1

		fi
	;;

	esac

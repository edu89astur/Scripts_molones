#!/bin/bash

#Usamos read para solicitar el nombre del fichero

read -p "Teclea el nombre de un fichero: " fichero

#Comprobamos que el fichero existe

	if [ -f "$fichero" ]; then

		echo "--------"
		echo "$fichero"
		echo "--------"
	else
		echo "$fichero No es un fichero"

		exit 1
	fi

#Comprobamos sus permisos

	if [ -r "$fichero" ]; then

		echo "Tiene permiso de lectura"
	else
		echo "No tiene permiso de lectura"
	fi

	if [ -w "$fichero" ]; then

		echo "Tiene permiso de escritura"
	else
		echo "No tiene permiso de escritura"
	fi

	if [ -x "$fichero" ]; then
		echo "Tiene permiso de ejecución"
	else
		echo "No tiene permiso de ejecución"
		exit 1
	fi

#!/bib/bash

#Preparación de las opciones que contendrá el menú

while true; do
	echo "-----------------------------------"
	echo "|            Menú:                 |"
	echo "| 1. Saludo                        |"
	echo "| 2. Calculadora                   |"
	echo "| 3. Crear directorio              |"
	echo "| 4. Crear usuario                 |"
	echo "| 5. ¿Qué tengo en mi directorio?  |"
	echo "| 6. Permisos                      |"
	echo "| 7. Escaneo IP                    |"
	echo "| 8. Crear enlace                  |"
	echo "| 9. Cambiar permisos              |"
	echo "| 0. Apagar o reiniciar            |"
	echo "| C. Cancelar menú                 |"
	echo "|__________________________________|"
#Llamados a otros scripts

ejecutar() {
		./calculadora.sh
}
directorio() {
		./crear-d.sh
}
permiso() {
		./permiso.sh
}
usuario() {
		./usuarios-c.sh
}
ip() {
		./servidores-v2.sh
}
apagar() {
		./sistema.sh
}
cambiar() {
		./cambiar.sh
}
enlace() {
		./enlace.sh
}
	read -p "Teclea un número del 0 al 9 o elige C para salir: " opcion

#Anidamos script y enlaces a cada opción usando case

	case $opcion in

	1)
	#Debemos crear 2 variables que registren la fecha y hora con un formato concreto
		fecha=$(date +"%d/%m/%Y")
		hora=$(date +"%H:%M:%S")
	#Creamos una variable que identifique al usuario
		usuario=$USER
	#Lanzamos echo con cada variable y saludo
		echo "-------------------------------------"
		echo "Hola, $usuario. Bienvenido al sistema"
		echo "-------------------------------------"
		echo "Hoy es $fecha"
		echo "-------------------------------------"
		echo "Y son las $hora"
		echo "-------------------------------------"
		exit 1
	;;
	2)	ejecutar
		exit 1
	;;

	3)	directorio
		exit 1
	;;
	4)	usuario
		exit 1
	;;
	5)	#Conseguir el nombre del usuario
		user=$(whoami)
		#Ruta del usuario
		diruser="/home/$user"
		#Listamos el contenido de la variable anterior
		echo "En tu directorio tienes: "
		ls -l "$diruser"
		exit 1
	;;
	6)	permiso
		exit 1
	;;
	7)	ip
		exit 1
	;;
	8)	enlace
		exit 1
	;;
	9)	cambiar
	;;
	0)	apagar
		exit 1
	;;
	c)
		echo "*******************************"
		echo "Gracias por usar el menú. Adiós"
		echo "*******************************"
		exit 0
	;;
	*)
		echo "************************************************************************************"
		echo "Esta opción no es válida. Por favor, teclea un número (1-9) o pulsa C para cancelar"
		echo "************************************************************************************"
	;;
	esac
done


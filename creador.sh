#!/bin/bash

#Verificamos que no exista el directorio.
directorio="$1"

if [ -d "$directorio" ]; then
	echo "El directorio "$directorio" ya existe."
exit 1
fi

mkdir "$directorio"



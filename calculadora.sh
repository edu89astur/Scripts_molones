#!/bin/bash

#Creación de la calculadora para incluirla sobre el menú

#Realizamos varias funciones

sumar() {
	resultado=$(echo "$1 + $2" | bc)
	echo "Resultado: $resultado"
}
restar() {
	resultado=$(echo "$1 - $2" | bc)
	echo "Resultado: $resultado"
}
multiplicar() {
	resultado=$(echo "$1 * $2" | bc)
	echo "Resultado: $resultado"
}
#scale=2 indica la cantidad de decimales que va a utilizar en los resultados
dividir() {
	resultado=$(echo "scale=2; $1 / $2" | bc)
	echo "Resultado: $resultado"
}

#Creamos opciones para la calculadora, como en un menu

echo "CALCULADORA"
echo "1. Sumar"
echo "2. Restar"
echo "3. Multiplicar"
echo "4. Dividir"
echo "5. Salir"

#Registro de opción del usuario

echo "Selecciona una opción del 1 al 5: "

read opcion

	case $opcion in

		1) read -p "Primer número: " n1
		   read -p "Segudno número: " n2
		   sumar $n1 $n2
		;;
		2) read -p "Primer número: " n1
		   read -p "Segundo número: " n2
		   restar $n1 $n2
		;;
		3) read -p "Primer número: " n1
		   read -p "Segundo número: " n2
		   multiplicar $n1 $n2
		;;
		4) read -p "Primer número: " n1
		   read -p "Segundo número: " n2
		   dividir $n1 $n2
		;;
		5) echo "Hasta luego"
		   exit 1
		;;
		*) echo "No has elegido una opción válida"
	esac

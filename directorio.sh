#!/bin/bash

#Petición de la ruta donde alojaremos el directorio

read -p "Teclea la ruta del directorio: " ruta

#Asignamos un nombre

read -p "Selecciona el nombre del directorio: " nombredir

#Creamos una variable que incluya los dos valores previos

rutadir="$ruta/$nombredir"

#Sería recomendable comprobar si el directorio ya existe para evitar problemas

if [ -d "$rutadir" ]; then
	echo "El directorio $rutadir ya existe."

else

#Si no existe el directorio, se crea

	mkdir -p "$rutadir"
	echo "***************************"
	echo "Directorio creado con éxito"
	echo "***************************"
fi

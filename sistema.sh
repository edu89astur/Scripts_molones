#!/bin/bash

#Estructura de menu

echo " ________________________ "
echo "| Elige una opción      |"
echo "| 1. Apagar el sistema  |"
echo "| 2. Reiniciar sistema  |"
echo " ------------------------"

#Desarrollamos el menú

read -p "Pulsa Enter después de escoger: " opcion

	case $opcion in

		1) #Apagar el sistema

		   echo "¡Hasta luego!"

		   #Agregamos un retraso para que se vea el mensaje

		   sleep 5
		   sudo shutdown now
		;;

		2) #Reiniciar el sistema

		   echo "Reiniciando el sistema."

		   #Agregamos un retraso para que se vea  el mensaje
		   sleep 5
		   sudo reboot
		;;

		*) #Error

		   echo "Te has equivocado de elección"
	           echo "Teclea 1 para apagar, 2 para reiniciar"
	esac

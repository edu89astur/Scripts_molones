#!/bin/bash

#Creamos dos variables que son los ficheros de txt para redirigir los resultados

conectados="conectados.txt"
nconectados="noconectados.txt"
#Limpiamos los ficheros conectados y no conectados
echo -n > $conectados
echo -n > $nconectados

#Realizamos un while para leer el fichero servidores.txt

while read ip; do
#Si usamos corchetes en este IF no funciona
	if  ping -c 1 -W 2 $ip >/dev/null 2>&1; then

		echo $ip >> $conectados
	else

		echo $ip >> $nconectados
	fi
done < servidores.txt

#Necesitamos que nos muestre en pantalla los listados creados

echo "----------------------------------------------------"
echo "Servidores con conexión"
cat $conectados

echo "-----------------------------------------------------"

echo "Servidores sin conexión"
cat $nconectados
echo "-----------------------------------------------------"

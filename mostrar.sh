#!/bin/bash

directorio="$1"
fichero="$2"
#Verificamos que se han introducido 2 parámetros, caso contrario reflejamos un error.
if [ $# -ne 2 ]; then
	echo "No has ingresado dos parámetros, has ingresado $#"
exit 1
fi
#Comprobamos que el primer parámetro es un directorio.
if [ -d "$directorio" ]; then
	echo "El directorio '$directorio' contiene:"
ls -l "$directorio"
else 
	echo "El parámetro introducido no es un directorio."
fi
#Comprobamos si el segundo parámetro es un fichero.
if [ -f "$fichero" ]; then
	echo "El fichero '$fichero' contiene:"
cat "$fichero"
else
	echo "El parámetro introducido no es un fichero."
fi

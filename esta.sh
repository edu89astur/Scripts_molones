#!/bin/bash

#Comprobamos número de argumentos

if [ $# -ne 1 ]; then
	echo "Número de argumentos incorrecto"
	echo "Sintaxis: $0 nombre-usuario"
exit 1

fi
#Nombre de usuario del primer y unico argumento

nombre_usuario="$1"

# Usamos `who` para verificar el usuario

if who | grep -q "$nombre_usuario"; then
	echo "$nombre_usuario esta conectado"
else
	echo "$nombre_usuario ni está ni se le espera"
fi

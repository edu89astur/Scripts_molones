#!/bin/bash

#Podrías hacer una comprobación de argumentos usando un if y -eq 0

# Creamos un bucle for, vamos también a comprobar si se introducen ficheros

for archivo in "$@"; do
	if [ -f "$archivo" ]; then

#Nombre de archivo y caracteres

nombrearchivo="$archivo"
numcaracteres=$(wc -c < "$archivo")

#Realizamos echo para mostrar la información

echo "Nombre del Fichero: $nombrearchivo"
echo "Numero de caracteres: $numcaracteres \n####################################"


else

#Si el archivo introducido no lo es, lanzamos mensaje de error

echo "El archivo '$archivo' no existe"

fi

done
#done se usa para cerrar bucles
